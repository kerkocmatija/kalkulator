from tkinter import *
from math import *
stevilka2=0
x=0
y=0
kos1=0
kos2=0
rezultat=0

n=0


class Kalkulator():
    def __init__(self, master):
        
        self.eee1 = IntVar(master, value=None)
        self.e1 = Entry(master, width=40, relief=RIDGE, justify='right')
        self.e1.grid(column=0, row=0, columnspan=3)
        self.eee2 = IntVar(master, value=None)
        self.e2 = Entry(master, width=26, relief=RIDGE, justify='right', textvariable=self.eee2)
        self.e2.grid(column=3, row=0, columnspan=2)
        
        b0=Button(master, text="0", height=5, width=22, command=self.napisi0)
        b1=Button(master, text="1", height=5, width=10, command=self.napisi1)
        b2=Button(master, text="2", height=5, width=10, command=self.napisi2)
        b3=Button(master, text="3", height=5, width=10, command=self.napisi3)
        b4=Button(master, text="4", height=5, width=10, command=self.napisi4)
        b5=Button(master, text="5", height=5, width=10, command=self.napisi5)
        b6=Button(master, text="6", height=5, width=10, command=self.napisi6)
        b7=Button(master, text="7", height=5, width=10, command=self.napisi7)
        b8=Button(master, text="8", height=5, width=10, command=self.napisi8)
        b9=Button(master, text="9", height=5, width=10, command=self.napisi9)
        b10=Button(master, text="POČISTI", height=5, width=10, command=self.pocisti)
        b11=Button(master, text="=", height=11, width=10, command=self.jeenako)
        b12=Button(master, text="POČISCTI VSE", height=5, width=10, command=self.pocisti_vse)
        b13=Button(master, text="+", height=5, width=10, command=self.sestevanje)
        b14=Button(master, text="-", height=5, width=10, command=self.odstevanje)
        b15=Button(master, text="*", height=5, width=10, command=self.mnozenje)
        b16=Button(master, text="/", height=5, width=10, command=self.deljenje)

        
        b0.grid(row=4, column=0, columnspan=2)
        b1.grid(row=3, column=0)
        b2.grid(row=3, column=1)
        b3.grid(row=3, column=2)
        b4.grid(row=2, column=0)
        b5.grid(row=2, column=1)
        b6.grid(row=2, column=2)
        b7.grid(row=1, column=0)
        b8.grid(row=1, column=1)
        b9.grid(row=1, column=2)
        b10.grid(row=1, column=4)
        b11.grid(row=2, column=4, rowspan=2)
        b12.grid(row=4, column=2)
        b13.grid(row=1, column=3)
        b14.grid(row=2, column=3)
        b15.grid(row=3, column=3)
        b16.grid(row=4, column=3)

    def pocisti(self):
        self.e1.delete(0, 'end')
        return None
    def pocisti_vse(self):
        self.e1.delete(0,'end')
        self.e2.delete(0,'end')
        return None

    def vpisi_stevilko(self, stevilka):
        self.e1.insert(0, stevilka)

    #FUNKCIJE ZA GUMBE S ŠTEVILKAMI
    def napisi0(self):
        self.e1.insert('end', '0')
    def napisi1(self):
        self.e1.insert('end', '1')
    def napisi2(self):
        self.e1.insert('end', '2')
    def napisi3(self):
        self.e1.insert('end', '3')
    def napisi4(self):
        self.e1.insert('end', '4')
    def napisi5(self):
        self.e1.insert('end', '5')
    def napisi6(self):
        self.e1.insert('end', '6')
    def napisi7(self):
        self.e1.insert('end', '7')
    def napisi8(self):
        self.e1.insert('end', '8')
    def napisi9(self):
        self.e1.insert('end', '9')

    #GUMBI Z OPERACIJAMI

    def sestevanje(self):
        stevilka1=self.e1.get()
        self.e1.insert('end', '+')
    def odstevanje(self):
        stevilka1=self.e1.get()
        self.e1.insert('end', '-')
    def mnozenje(self):
        stevilka1=self.e1.get()
        self.e1.insert('end', '*')
    def deljenje(self):
        stevilka1=self.e1.get()
        self.e1.insert('end', '/')

    #GUMB =

    def jeenako(self):
        with open('rezultati.txt', 'a+') as f:
            stevilka2=self.e1.get()
            abc=stevilka2[0]
            #self.e1.delete(0, 'end')
            for i in stevilka2:
                if i=='+':
                    x=stevilka2.index('+')
                    kos1=int(stevilka2[:x])
                    kos2=int(stevilka2[x+1:])
                    self.eee2.set(kos1+kos2)
                    if kos1 >=0 and kos2 >=0:
                        self.eee2.set(kos1+kos2)
                    if kos1 >=0 and kos2<0:
                        self.eee2.set(kos1+kos2)
                    if kos1 <0 and kos2>=0:
                        self.eee2.set(kos2+kos1)
                    if kos1 <0 and kos2 <0:
                        self.eee2.set(kos1+kos2)
###########################################################################################                      
                elif i=='*':
                    x=stevilka2.index('*')
                    kos1=int(stevilka2[:x])
                    kos2=int(stevilka2[x+1:])
                    self.eee2.set(kos1+kos2)
                    if kos1 >=0 and kos2 >=0:
                        self.eee2.set(kos1*kos2)
                    elif kos1 >=0 and kos2<0:
                        self.eee2.set(kos1*kos2)
                    elif kos1 <0 and kos2>=0:
                        self.eee2.set(kos1*kos2)
                    elif kos1 <0 and kos2<0:
                        self.eee2.set(kos1*kos2)
###########################################################################################                        
                elif i=='/':
                    x=stevilka2.index('/')
                    kos1=int(stevilka2[:x])
                    kos2=int(stevilka2[x+1:])
                    self.eee2.set(kos1+kos2)
                    if kos1 >=0 and kos2 >=0:
                        self.eee2.set(kos1/kos2)
                    elif kos1 >=0 and kos2<0:
                        self.eee2.set(kos1/kos2)
                    elif kos1 <0 and kos2>=0:
                        self.eee2.set(kos1/kos2)
                    elif kos1 <0 and kos2<0:
                        self.eee2.set(kos1/kos2)
######################################################################################## 
            stevilka3=stevilka2[1:]
            if '-' in stevilka3 and '+' and '*' and '/' not in stevilka3:
                x=stevilka3.index('-')
                kos1=int(stevilka2[:x+1])
                kos2=int(stevilka2[x+2:])
                self.eee2.set(kos1+kos2)
                if kos1 >=0 and kos2 >=0:
                    self.eee2.set(kos1-kos2)
                elif kos1 >=0 and kos2<0:
                    self.eee2.set(kos1-kos2)
                elif kos1 <0 and kos2>=0:
                    self.eee2.set(kos1-kos2)
                elif kos1 <0 and kos2<0:
                    self.eee2.set(kos1-kos2)
                
                                         
    
            f.write('REZULTAT RACUNA: {0}={1}\n'.format(self.e1.get(),self.e2.get()))
            f.close()
            
      
root=Tk()
aplikacija=Kalkulator(root)
root.title('Kalkulator')  
root.mainloop()
